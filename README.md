# Setup

## Install OS/database

Install on virtualbox a debian 9 with ssh.

Install percona server 5.7.

As root user run
* wget https://repo.percona.com/apt/percona-release_latest.$(lsb_release -sc)_all.deb
* dpkg -i percona-release_latest.$(lsb_release -sc)_all.deb
* apt-get update
* apt-get install percona-server-server-5.7

During the install process set the database's root user password.

If the server has to be accessible from other hosts edit `/etc/mysql/percona-server.conf.d/mysqld.cnf`
and set the parameter `bind-address = 0.0.0.0`, restart the mysql service to apply the change.

systemctl restart mysql


## Setup database user

Login as root and run the following statements.

CREATE DATABASE db_test;
CREATE USER usr_test ;
ALTER USER usr_test IDENTIFIED BY 'test';
GRANT ALL ON db_test.* TO usr_test;
FLUSH PRIVILEGES;

Check the connection is possible.

# Tests

## Download the dataset

Download the dataset http://www2.informatik.uni-freiburg.de/~cziegler/BX/

    cd data
    wget http://www2.informatik.uni-freiburg.de/~cziegler/BX/BX-SQL-Dump.zip
    unzip BX-SQL-Dump.zip
    Archive:  BX-SQL-Dump.zip
     inflating: BX-Users.sql
     inflating: BX-Book-Ratings.sql
     inflating: BX-Books.sql

## load the data into the database
The create table use the mysql 4.x syntax for the table type.
Replacing TYPE with ENGINE makes the create table work correctly.

The sql files are latin1 encoded which makes some special characters fail when processed by mysql.

    INSERT INTO `BX-Book-Ratings` VALUES (11676,'8475560806º',6);

Is then necessary to convert the files to utf8 using iconv.

    for f in `ls *.sql`
    do
    echo "converting "$f
    futf=`echo $f| awk -F'.' '{print $1}'`_utf8.sql
    iconv -f latin1 -t utf-8 -o $futf $f
    done


The files can be loaded using a simple redirect.
mysql -u usr_test -h host_percona_server -p db_test < BX-Book-Ratings_utf8.sql
mysql -u usr_test -h host_percona_server -p db_test < BX-Books_utf8.sql
mysql -u usr_test -h host_percona_server -p db_test < BX-Users_utf8.sql

## generate top 10 books per each country into a separate table.

The script which does the task is in the file top_10.sql.

The logic behind is to use a destination table `top_ten_country` as requested and a function to loop over the results for building the top 10 per each country.

A temporary table  `t_loc_users`  is used to extract the country from the field `BX-Users`.`Location`.
A quick analysis of the field's contents shows that the last element of the comma separated value is the country.

Using the nested functions `TRIM(BOTH  FROM (TRIM(BOTH '.' FROM SUBSTRING_INDEX(Location, ",", -1))))` the field Location's last element is trimmed from the spaces and eventual dots. The value is stored into the `t_loc_users` with the users id.

The function then declares a cursor selecting the country from `t_loc_users` and for each country inserts into the `top_ten_country`  the top 10 books for the country joining the corresponding users belonging to that country.

An index on  `t_loc_users` should improve the performances.

However, the field Location is full of rubbish data and should be analysed more in details to find a correct way to determine the country.

The join is not very efficient and the function's call takes long time for building the requested dataset.

This initial approach requires improvement for sure.
