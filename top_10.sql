DROP TABLE IF EXISTS top_ten_country;
CREATE TABLE top_ten_country
(
	`ISBN` varchar(13),
	`Book-Title` varchar(255),
	`Book-Author` varchar(255),
	`Book-Rating` int(4),
	`Book-Country` varchar(255)
)
ENGINE=MyIsam;

DROP PROCEDURE IF EXISTS GENTOPTEN;
DELIMITER ;;

CREATE PROCEDURE GENTOPTEN()
BEGIN
	DECLARE v_country VARCHAR(255) DEFAULT 0;
	DECLARE loop_done INT DEFAULT 0;
	DECLARE location_cursor CURSOR FOR  SELECT DISTINCT `Country` FROM t_loc_users;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET loop_done = 1;
	TRUNCATE TABLE  top_ten_country;
	DROP TABLE IF EXISTS t_loc_users;
	CREATE TEMPORARY TABLE t_loc_users AS
	SELECT  `User-ID`,Location, TRIM(BOTH  FROM (TRIM(BOTH '.' FROM SUBSTRING_INDEX(Location, ",", -1)))) as Country FROM db_test.`BX-Users` usr;

	CREATE INDEX idx_loc_usr ON t_loc_users (`User-ID`);

	OPEN location_cursor;
	SET loop_done = 0;
	 REPEAT
	    FETCH location_cursor INTO v_country;

	   INSERT INTO top_ten_country
			SELECT
				bk.ISBN,
				bk.`Book-Title`,
				bk.`Book-Author`,
				rat.`Book-Rating`,
				usr.`Country` as `Book-Country`
			FROM
				db_test.`BX-Books` bk
				INNER JOIN 	db_test.`BX-Book-Ratings` rat
				ON bk.ISBN=rat.ISBN
				INNER JOIN t_loc_users usr
				ON usr.`User-ID`=rat.`User-ID`
			WHERE usr.Country=v_country
			order by rat.`Book-Rating`desc
			limit 10
			;

	 UNTIL loop_done END REPEAT;

	 CLOSE location_cursor;


END ;;


DELIMITER ;

CALL GENTOPTEN();
